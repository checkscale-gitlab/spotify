# Librespot client on alpine for the Raspberry Pi

Small Open source alpine RPI client ( ~5Mo ) to run on a raspberry pi. There's also a curl version to use with an API

## source
source: https://gitlab.com/easy-docker/spotify \
based on : https://github.com/librespot-org/librespot

## script
you can provide a script, you can found an example on the git project

## docker
### You quickly want to try 
it to see if it works... run this on your raspberry \
`docker run --name librespot-test --rm -d --device=/dev/snd:/dev/snd --network=host --privileged flori4n/librespot:latest librespot --name "librespot-test"` \
to remove it \
`docker stop librespot-test`

## docker compose

```yml
version: "3.8"
services:
  spotify:
    name: librespot
    privileged: true
    image: flori4n/librespot:latest
    network_mode: host
    working_dir: /data
    restart: unless-stopped
    volumes:
     - $PWD/script.sh:/data/script.sh
    devices:
     - /dev/snd:/dev/snd:rw
    command: librespot /
             --enable-volume-normalisation /
             --onevent /data/script.sh /
             --name "Raspotify" /
             --bitrate 320 /
             --device-type=speaker /
             --disable-audio-cache /
             --initial-volume 60 /
             --disable-gapless /
             --autoplay /
             --initial-volume=70
```
